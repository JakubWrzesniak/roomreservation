package com.example.roomreservation.repository;

import com.example.roomreservation.entity.Reservation;
import com.example.roomreservation.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

public interface ReservationDAO extends JpaRepository<Reservation, Long> {
    List<Reservation> findByRoom_RoomIdEquals(Long roomId);


    Set<Reservation> findByRoom_RoomIdEqualsAndStartDateGreaterThanEqualAndEndDateLessThanEqual(@NotNull Long roomId, Timestamp startDate,
                                                                           Timestamp endDate);

    Set<Reservation> findByOwnerEqualsAndStartDateGreaterThanEqual(User owner, Timestamp startDate);



    Set<Reservation> findByRoom_RoomIdEqualsAndStartDateLessThanEqualAndEndDateGreaterThanEqual(@NotNull Long roomId, Timestamp startDate, Timestamp endDate);


    Set<Reservation> findByRoom_RoomIdEqualsAndStartDateGreaterThanEqualAndStartDateLessThanEqual(@NotNull Long roomId, Timestamp startDate, Timestamp startDate1);

    Set<Reservation> findByRoom_RoomIdAndStartDateEqualsAndEndDateEquals(@NotNull Long roomId, Timestamp startDate, Timestamp endDate);

    Set<Reservation> findByRoom_RoomIdEqualsAndStartDateGreaterThanAndStartDateLessThanOrStartDateLessThanAndEndDateGreaterThanOrStartDateLessThanAndEndDateGreaterThanOrStartDateGreaterThanEqualAndEndDateLessThanEqual(
            Long roomId, Timestamp startDate, Timestamp startDate1, Timestamp startDate2, Timestamp endDate,
            Timestamp startDate3, Timestamp endDate1, Timestamp startDate4, Timestamp endDate2);


}
