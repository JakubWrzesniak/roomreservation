package com.example.roomreservation.repository;

import com.example.roomreservation.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;

public interface UserDAO extends JpaRepository<User, Long> {
    Optional<User> findByEmailAddressEqualsIgnoreCase(String emailAddress);

    Optional<User> findByUserAzureIdEquals(String userAzureId);


}
