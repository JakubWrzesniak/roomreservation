package com.example.roomreservation.repository;

import com.example.roomreservation.entity.Floor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface FloorDAO extends CrudRepository<Floor, Long> {
}
