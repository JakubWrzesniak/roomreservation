package com.example.roomreservation.repository;

import com.example.roomreservation.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface RoomDAO extends CrudRepository<Room, Long> {
    Set<Room> findByFloor_NumberEquals(Long number);

}
