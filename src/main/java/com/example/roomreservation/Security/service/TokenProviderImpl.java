package com.example.roomreservation.Security.service;

import com.auth0.jwk.*;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.Map;

@Slf4j
@Configuration
public class TokenProviderImpl implements TokenProvider {
    private final String JWK_PROVIDER_URL = "https://login.microsoftonline.com/f757dd37-d403-4fbc-8e26-6c3a256405d0/discovery/v2.0/keys";

    @Override
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, "preferred_username").asString();
    }
    public Claim getClaimFromToken(String token, String claimName) {
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getClaim(claimName);
    }

    @Override
    public String getName(String token){
        return getClaimFromToken(token, "name").asString();
    }

    @Override
    public Map<String, Claim> getClaimsFromToken(String token) {
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getClaims();
    }

    @Override
    public Date getExpiryDateFromToken(String token) {
        DecodedJWT jwt = JWT.decode(token);
        return jwt.getExpiresAt();
    }

    @Override
    public boolean validateToken(String token) {
        try {
            // decode it
            DecodedJWT jwt = JWT.decode(token);

            Jwk         jwk;
            JwkProvider provider;
            Algorithm   algorithm;
            try {
                provider = new UrlJwkProvider(new URL(JWK_PROVIDER_URL));
                jwk = provider.get(jwt.getKeyId());
                algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);
                algorithm.verify(jwt);
                Date now = new Date();
                Date notBefore = jwt.getNotBefore();
                Date expiresAt = getExpiryDateFromToken(token);
                return notBefore != null && expiresAt != null
                        && now.toInstant().compareTo(notBefore.toInstant()) >= 0
                        && now.toInstant().isBefore(expiresAt.toInstant()); // valid
            } catch (MalformedURLException | JwkException | SignatureVerificationException e) {
                log.warn(e.getMessage(), e);
                return false; // invalid
            }
            // Jwts.parser().setSigningKey(tokenSecret).parse(token);
        } catch (SignatureException | ExpiredJwtException | UnsupportedJwtException | MalformedJwtException |
                 IllegalArgumentException ex) {
            log.error(ex.getMessage());
        }
        return false;
    }
}
