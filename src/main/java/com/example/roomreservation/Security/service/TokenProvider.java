package com.example.roomreservation.Security.service;

import com.auth0.jwt.interfaces.Claim;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TokenProvider {
    String getUsernameFromToken(String token);
    String getName(String token);
    Object getClaimFromToken(String token, String claimName);

    Date getExpiryDateFromToken(String token);

    boolean validateToken(String token);
    Map<String, Claim> getClaimsFromToken(String token);
    static String getTokenFromHeader(String token){
        if (StringUtils.hasText(token) && token.startsWith("Bearer ")) {
            return token.substring(7);
        }
        return null;
    }
}
