//package com.example.roomreservation.Security.Filter;
//
//import com.example.roomreservation.Security.Util.SecurityCipher;
//import com.example.roomreservation.Security.service.TokenProvider;
//import com.example.roomreservation.Services.UserService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
//import org.springframework.util.StringUtils;
//import org.springframework.web.filter.OncePerRequestFilter;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//import static java.util.Arrays.stream;
//
//@Slf4j
//public class TokenAuthenticationFilter extends OncePerRequestFilter {
//
//    @Value("${spring.security.auth.accessTokenCookieName}")
//    private String accessTokenCookieName;
//
//    @Value("${spring.security.auth.refreshTokenCookieName}")
//    private String refreshTokenCookieName;
//
//    @Autowired
//    private UserService userService;
//    @Autowired
//    private TokenProvider tokenProvider;
//
//    private String getJwtFromRequest(HttpServletRequest request) {
//        String bearerToken = request.getHeader("authorization");
//        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
//            String accessToken = bearerToken.substring(7);
//            if (accessToken == null) return null;
//            return accessToken;
//        }
//        return null;
//    }
//    private String getJwtFromCookie(HttpServletRequest request) {
//        Cookie[] cookies = request.getCookies();
//        if(cookies!=null) {
//            for (Cookie cookie : cookies) {
//                if (accessTokenCookieName.equals(cookie.getName())) {
//                    String accessToken = cookie.getValue();
//                    if (accessToken == null) return null;
//                    return SecurityCipher.decrypt(accessToken);
//                }
//            }
//        }
//        return null;
//    }
//    private String getJwtToken(HttpServletRequest request, boolean fromCookie) {
//        if (fromCookie) return getJwtFromCookie(request);
//
//        return getJwtFromRequest(request);
//    }
//    @Override
//    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
//                                    FilterChain filterChain) throws ServletException, IOException {
//        if(!request.getServletPath().equals("/auth/login") && !request.getServletPath().equals("/auth/refresh")){
//            try {
//                String jwt = getJwtToken(request, false);
//                if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
//                    String       username    = tokenProvider.getUsernameFromToken(jwt);
//                    UserDetails  userDetails = userService.loadUserByUsername(username);
//                    UsernamePasswordAuthenticationToken authentication =
//                            new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
//                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//                    SecurityContextHolder.getContext().setAuthentication(authentication);
//                }
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
//        filterChain.doFilter(request, response);
//    }
//}
