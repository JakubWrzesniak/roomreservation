package com.example.roomreservation;

import com.example.roomreservation.ModelDTO.RoomCreateDTO;
import com.example.roomreservation.Security.WebMvcConfig;
import com.example.roomreservation.Security.service.TokenProvider;
import com.example.roomreservation.Security.service.TokenProviderImpl;
import com.example.roomreservation.Services.FloorService;
import com.example.roomreservation.Services.RoomService;
import com.example.roomreservation.Services.UserService;
import com.example.roomreservation.entity.Floor;
import com.example.roomreservation.entity.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.HashSet;


@SpringBootApplication
//@EnableSwagger2
public class RoomReservationApplication{
    public static void main(String[] args) {
        SpringApplication.run(RoomReservationApplication.class, args);
    }

    @Bean
    TokenProvider tokenProvider(){
        return new TokenProviderImpl();
    }
    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer(){
        return new WebMvcConfig();
    }

//    @Bean
//    CommandLineRunner run(UserService userService, FloorService floorService, RoomService roomService) {
//        return args -> {
//            Floor floor = new Floor(1L, 500, 800, new HashSet<>());
//            floorService.saveFloor(floor);
//            RoomCreateDTO room1 = new RoomCreateDTO( 1L , "1.1", "Meet Me",629.0, 18.0, 153.0, 105.0);
//            RoomCreateDTO room2 = new RoomCreateDTO( 1L, "1.2", "Conference Room", 424.0, 18.0, 181.0, 105.0);
//            RoomCreateDTO room3 = new RoomCreateDTO(1L, "1.3", "Conference Room 1",227.0, 18.0, 173.0, 105.0);
//            RoomCreateDTO room4 = new RoomCreateDTO(1L, "1.3", "Conference Room 2",20.0, 18.0, 183.0, 105.0);
//            RoomCreateDTO room5 = new RoomCreateDTO(1L, "1.3", "Conference Room 3",20.0, 141.0, 159.0, 201.0);
//            RoomCreateDTO room6 = new RoomCreateDTO(1L, "1.3", "Conference Room 4",20.0, 363.0, 315.0, 126.0);
//            RoomCreateDTO room7 = new RoomCreateDTO(1L, "1.3", "Conference Room 5",355.0, 273.0, 145.0, 216.0);
//            RoomCreateDTO room8 = new RoomCreateDTO(1L, "1.3", "Conference Room 6",515.0, 273.0, 130.0, 216.0);
//            RoomCreateDTO room9 = new RoomCreateDTO(1L, "1.3", "Conference Room 7",664.0, 358.0, 118.0, 131.0);
//            roomService.saveRoom(room1);
//            roomService.saveRoom(room2);
//            roomService.saveRoom(room3);
//            roomService.saveRoom(room4);
//            roomService.saveRoom(room5);
//            roomService.saveRoom(room6);
//            roomService.saveRoom(room7);
//            roomService.saveRoom(room8);
//            roomService.saveRoom(room9);
//        };
//    }
}
