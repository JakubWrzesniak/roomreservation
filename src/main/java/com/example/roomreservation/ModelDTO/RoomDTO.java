package com.example.roomreservation.ModelDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoomDTO {
    private Long roomId;
    private Long floor;
    private String number;
    private String name;
    private Double x;
    private Double y;
    private Double width;
    private Double height;
}
