package com.example.roomreservation.ModelDTO;

import com.example.roomreservation.entity.Room;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReservationDtoDetailsWithRoom {
    private Long                  id;
    private String                ownerEmail;
    private String                  roomNumber;
    private String roomName;
    private java.util.Set<String> participantUsers;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp startDate;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp endDate;
}
