package com.example.roomreservation.ModelDTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReservationRoomDto implements Serializable {
    private Long roomId;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp startDate;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp endDate;
}
