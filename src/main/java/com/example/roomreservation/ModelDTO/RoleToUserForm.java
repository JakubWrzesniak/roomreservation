package com.example.roomreservation.ModelDTO;

import lombok.Data;

@Data
public class RoleToUserForm {
    private String userEmail;
    private String role;
}
