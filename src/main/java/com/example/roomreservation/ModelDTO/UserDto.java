package com.example.roomreservation.ModelDTO;

import lombok.*;

import javax.validation.constraints.Email;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    private Long userId;
    private String username;
    @Email
    private String emailAddress;
}
