package com.example.roomreservation.ModelDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IsRoomReserved {
    private Long roomId;
    private Boolean isReserved;
}
