package com.example.roomreservation.ModelDTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FloorReservationsInDatesDto {
    private Long      floorNumber;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp startDate;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp endDate;
}
