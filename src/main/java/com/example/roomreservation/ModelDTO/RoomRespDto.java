package com.example.roomreservation.ModelDTO;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
public class RoomRespDto implements Serializable {
    private Long   roomId;
    private Long   floorNumber;
    @NotNull
    @NotEmpty
    private String number;
    @NotEmpty
    private String name;
    @Min(0)
    @Max(2000)
    private Double x;
    @Min(0)
    @Max(2000)
    private Double y;
    @Min(0)
    @Max(2000)
    private Double width;
    @Min(0)
    @Max(2000)
    private Double height;
}
