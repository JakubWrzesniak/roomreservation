package com.example.roomreservation.ModelDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FloorDto {
    private Long number;
    private Integer height;
    private Integer width;
    private Set<RoomRespDto> rooms;
}
