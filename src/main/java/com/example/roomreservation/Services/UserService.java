package com.example.roomreservation.Services;

import com.example.roomreservation.ModelDTO.UserDto;
import com.example.roomreservation.Security.service.TokenProvider;
import com.example.roomreservation.entity.User;
import com.example.roomreservation.repository.UserDAO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserService implements IUserService {
    //    @Autowired
    //    private Environment env;
    //
    //    @Autowired
    //    private LdapTemplate ldapTemplate;
    private final UserDAO userDAO;
    private final TokenProvider   tokenProvider;

    private final ModelMapper modelMapper;


    @Override
    public User saveUser(User user) {
        log.info("Saving new user into database: {}", user.getEmailAddress());
        return userDAO.save(user);
    }

    @Override
    public User getUser(String username) {
        log.info("Fetching user {}", username);
        return userDAO.findByEmailAddressEqualsIgnoreCase(username).orElse(null);
    }

    public Collection<UserDto> getUsers() {
        log.info("Fetching all users");
        var users = userDAO.findAll();
        var mappedUsers = users.stream().map( u -> modelMapper.map(u, UserDto.class)).toList();
        return mappedUsers;
    }

    @Override
    public boolean authenticateUser(String token) {
        token = TokenProvider.getTokenFromHeader(token);
        if(tokenProvider.validateToken(token)){
            String userId = tokenProvider.getClaimFromToken(token, "sub").toString();
            var user = userDAO.findByUserAzureIdEquals(userId);
            String tokenUserEmail = tokenProvider.getUsernameFromToken(token);
            if(user.isPresent()){
                if(!tokenUserEmail.equals(user.get().getEmailAddress())){
                    user.get().setEmailAddress(tokenUserEmail);
                    userDAO.save(user.get());
                }
            } else {
                User newUser = new User();
                newUser.setUserAzureId(userId);
                newUser.setEmailAddress(tokenUserEmail);
                newUser.setUsername(tokenProvider.getName(token));
                userDAO.save(newUser);
            }
            return true;
        }
        return false;
    }

}
