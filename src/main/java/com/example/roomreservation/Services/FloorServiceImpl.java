package com.example.roomreservation.Services;

import com.example.roomreservation.ModelDTO.FloorDto;
import com.example.roomreservation.entity.Floor;
import com.example.roomreservation.repository.FloorDAO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class FloorServiceImpl implements FloorService {
    private final FloorDAO floorDAO;
    private final ModelMapper modelMapper;

    public FloorServiceImpl(FloorDAO floorDAO, ModelMapper modelMapper) {
        this.floorDAO = floorDAO;
        this.modelMapper = modelMapper;
    }

    @Override
    public Floor saveFloor(Floor floor) {
        return floorDAO.save(floor);
    }

    @Override
    public FloorDto getFloor(Long number) {
        var floorDetails = floorDAO.findById(number).orElseThrow();
        return modelMapper.map(floorDetails, FloorDto.class);
    }
}
