package com.example.roomreservation.Services;


import com.example.roomreservation.ModelDTO.*;

import java.util.Collection;

public interface ReservationService {
    Collection<ReservationDtoDetails>  getReservations(Long room_id);
    Collection<IsRoomReserved>  getReservations(FloorReservationsInDatesDto floorReservationsInDatesDto);
    Collection<ReservationDtoDetails>  getReservations(ReservationRoomDto reservationRoomDto);
    Collection<ReservationDtoDetailsWithRoom> getFutureUserReservations(String username);
    ReservationDtoDetails saveReservation(ReservationDto reservationDto, String owner);
}
