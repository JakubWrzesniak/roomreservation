package com.example.roomreservation.Services;

import com.example.roomreservation.ModelDTO.RoomCreateDTO;
import com.example.roomreservation.ModelDTO.RoomRespDto;
import com.example.roomreservation.entity.Room;

import java.util.Collection;

public interface IRoomService {
    Collection<RoomRespDto> getRooms();

    RoomRespDto getRoom(long id);


    public Room saveRoom(RoomCreateDTO newRoom);
}
