package com.example.roomreservation.Services;

import com.example.roomreservation.ModelDTO.*;
import com.example.roomreservation.entity.Reservation;
import com.example.roomreservation.entity.Room;
import com.example.roomreservation.entity.User;
import com.example.roomreservation.repository.FloorDAO;
import com.example.roomreservation.repository.ReservationDAO;
import com.example.roomreservation.repository.RoomDAO;
import com.example.roomreservation.repository.UserDAO;

import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl implements ReservationService{
    private final   ReservationDAO reservationDAO;
    private final FloorDAO floorDAO;
    private final RoomDAO roomDAO;
    private final UserDAO userDAO;

    public ReservationServiceImpl(ReservationDAO reservationDAO, FloorDAO floorDAO, RoomDAO roomDAO, UserDAO userDAO) {
        this.reservationDAO = reservationDAO;
        this.floorDAO = floorDAO;
        this.roomDAO = roomDAO;
        this.userDAO = userDAO;
    }

    @Override
    public Collection<ReservationDtoDetails> getReservations(Long room_id) {
        var reservations =  reservationDAO.findByRoom_RoomIdEquals(room_id);
        return reservations.stream().map(this::mapReservationToDTODetails).toList();
    }

    @Override
    public Collection<IsRoomReserved> getReservations(FloorReservationsInDatesDto floorReservationsInDatesDto) {
        var rooms = roomDAO.findByFloor_NumberEquals(floorReservationsInDatesDto.getFloorNumber());
        var roomReservations = rooms.stream().map(room ->
                new IsRoomReserved(room.getRoomId(), !getReservations(new ReservationRoomDto(room.getRoomId(), floorReservationsInDatesDto.getStartDate(), floorReservationsInDatesDto.getEndDate())).isEmpty())
        ).collect(Collectors.toSet());
        return roomReservations;
    }

    @Override
    public Collection<ReservationDtoDetails> getReservations(ReservationRoomDto reservationRoomDto) {
        var reservations = reservationDAO.findByRoom_RoomIdEqualsAndStartDateGreaterThanEqualAndEndDateLessThanEqual(reservationRoomDto.getRoomId(), reservationRoomDto.getStartDate(), reservationRoomDto.getEndDate());
        var firstCondition = reservationDAO.findByRoom_RoomIdEqualsAndStartDateGreaterThanEqualAndStartDateLessThanEqual(reservationRoomDto.getRoomId(), reservationRoomDto.getStartDate(), reservationRoomDto.getEndDate());
        var secondCondition = reservationDAO.findByRoom_RoomIdEqualsAndStartDateLessThanEqualAndEndDateGreaterThanEqual(reservationRoomDto.getRoomId(), reservationRoomDto.getStartDate(), reservationRoomDto.getStartDate());

        reservations.addAll(firstCondition);
        reservations.addAll(secondCondition);
//        var reservations = reservationDAO.findByRoom_RoomIdEqualsAndStartDateGreaterThanAndStartDateLessThanOrStartDateLessThanAndEndDateGreaterThanOrStartDateLessThanAndEndDateGreaterThanOrStartDateGreaterThanEqualAndEndDateLessThanEqual(
//                reservationRoomDto.getRoomId(),
//                reservationRoomDto.getStartDate(),
//                reservationRoomDto.getEndDate(),
//                reservationRoomDto.getStartDate(),
//                reservationRoomDto.getEndDate(),
//                reservationRoomDto.getStartDate(),
//                reservationRoomDto.getStartDate(),
//                reservationRoomDto.getStartDate(),
//                reservationRoomDto.getEndDate());
        return reservations.stream().map(this::mapReservationToDTODetails).toList();
    }

    @Override
    public Collection<ReservationDtoDetailsWithRoom> getFutureUserReservations(String username) {
        LocalDateTime localDateTime = LocalDateTime.now();
        User        ownerUser       = userDAO.findByEmailAddressEqualsIgnoreCase(username).orElseThrow(() -> new IllegalArgumentException("User not found with email " + username));
        var reservations = reservationDAO.findByOwnerEqualsAndStartDateGreaterThanEqual(ownerUser,  Timestamp.valueOf(localDateTime));
        return reservations.stream().map(this::mapReservationWithRoomDetailsDTODetails).sorted(Comparator.comparing(ReservationDtoDetailsWithRoom::getStartDate)).toList();
    }

    @Override
    public ReservationDtoDetails saveReservation(ReservationDto reservationDto, String owner) {
        if(reservationDto.getStartDate().before(Timestamp.valueOf(LocalDateTime.now()))){
            throw new IllegalArgumentException("Cannot make reservation in the past");
        }
        if(!reservationDto.getEndDate().after(reservationDto.getStartDate())){
            throw new IllegalArgumentException("End date has to be after start date");
        }
        if(reservationDto.getParticipantUsers().contains(owner)){
            throw new IllegalArgumentException("Owner can't be a member of the meeting");
        }
        Room        room        = roomDAO.findById(reservationDto.getRoomId()).orElseThrow(() -> new IllegalArgumentException("Room not found with id " + reservationDto.getRoomId()));
        User        ownerUser       = userDAO.findByEmailAddressEqualsIgnoreCase(owner).orElseThrow(() -> new IllegalArgumentException("User not found with email " + owner));
        var firstCondition = reservationDAO.findByRoom_RoomIdEqualsAndStartDateGreaterThanEqualAndStartDateLessThanEqual(reservationDto.getRoomId(), reservationDto.getStartDate(), reservationDto.getEndDate());
        var secondCondition = reservationDAO.findByRoom_RoomIdEqualsAndStartDateLessThanEqualAndEndDateGreaterThanEqual(reservationDto.getRoomId(), reservationDto.getStartDate(), reservationDto.getStartDate());
        var thirdCondition = reservationDAO.findByRoom_RoomIdAndStartDateEqualsAndEndDateEquals(reservationDto.getRoomId(), reservationDto.getStartDate(), reservationDto.getEndDate());
        if(!firstCondition.isEmpty() || !secondCondition.isEmpty() || !thirdCondition.isEmpty()){
            throw new IllegalArgumentException(MessageFormat.format("Room is reserved in this dates {0}, {1}", reservationDto.getStartDate(), reservationDto.getEndDate()));
        }
        Reservation reservation = new Reservation();
        reservation.setRoom(room);
        reservation.setOwner(ownerUser);
        reservation.setEndDate(reservationDto.getEndDate());
        reservation.setStartDate(reservationDto.getStartDate());
        Set<User>   users       = reservationDto.getParticipantUsers().stream().map(u -> userDAO.findByEmailAddressEqualsIgnoreCase(u).orElseThrow(() -> new IllegalArgumentException("User not found with email " + u))).collect(
                Collectors.toSet());
        reservation.setParticipants(users);
        Reservation newReservation =  reservationDAO.save(reservation);
        return  mapReservationToDTODetails(newReservation);
    }

    public ReservationDtoDetails mapReservationToDTODetails(Reservation reservation){
        return new ReservationDtoDetails(reservation.getReservationId(), reservation.getOwner()
                .getEmailAddress(), reservation.getRoom().getRoomId(), reservation.getParticipants().stream().map(User::getEmailAddress).collect(Collectors.toSet()), reservation.getStartDate(), reservation.getEndDate());

    }

    public ReservationDtoDetailsWithRoom mapReservationWithRoomDetailsDTODetails(Reservation reservation){
        return new ReservationDtoDetailsWithRoom(reservation.getReservationId(), reservation.getOwner()
                .getEmailAddress(), reservation.getRoom().getNumber(), reservation.getRoom().getName(), reservation.getParticipants().stream().map(User::getEmailAddress).collect(Collectors.toSet()), reservation.getStartDate(), reservation.getEndDate());

    }
}
