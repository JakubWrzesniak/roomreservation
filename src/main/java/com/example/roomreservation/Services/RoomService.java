package com.example.roomreservation.Services;

import com.example.roomreservation.ModelDTO.RoomCreateDTO;
import com.example.roomreservation.ModelDTO.RoomRespDto;
import com.example.roomreservation.entity.Floor;
import com.example.roomreservation.entity.Room;
import com.example.roomreservation.repository.FloorDAO;
import com.example.roomreservation.repository.RoomDAO;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class RoomService implements IRoomService{

    private final RoomDAO roomDAO;
    private final FloorDAO floorDAO;

    private final ModelMapper modelMapper;

    public RoomService(RoomDAO roomDAO, FloorDAO floorDAO, ModelMapper modelMapper) {
        this.roomDAO = roomDAO;
        this.floorDAO = floorDAO;
        this.modelMapper = modelMapper;
    }

    @Override
    public Collection<RoomRespDto> getRooms() {
        ArrayList<RoomRespDto> rooms = new ArrayList<>();
        roomDAO.findAll().forEach(r -> rooms.add(modelMapper.map(r, RoomRespDto.class)));
        return rooms;
    }

    @Override
    public RoomRespDto getRoom(long id) {
        return modelMapper.map(roomDAO.findById(id).orElseThrow(), RoomRespDto.class) ;
    }

    @Override
    public Room saveRoom(RoomCreateDTO newRoom) {
        Floor floor = floorDAO.findById(newRoom.getFloor()).orElseThrow();
        Room room = modelMapper.map(newRoom, Room.class);
        room.setFloor(floor);
        return roomDAO.save(room);
    }
}
