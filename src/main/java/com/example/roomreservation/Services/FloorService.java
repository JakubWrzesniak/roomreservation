package com.example.roomreservation.Services;

import com.example.roomreservation.ModelDTO.FloorDto;
import com.example.roomreservation.entity.Floor;

public interface FloorService {
    Floor saveFloor(Floor floor);
    FloorDto getFloor(Long number);
}
