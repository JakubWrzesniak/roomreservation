package com.example.roomreservation.Services;

import com.example.roomreservation.ModelDTO.UserDto;
import com.example.roomreservation.entity.User;

import java.util.Collection;

public interface IUserService {
    User saveUser(User user);
    User getUser(String username);
    Collection<UserDto> getUsers();
    boolean authenticateUser(String token);
}
