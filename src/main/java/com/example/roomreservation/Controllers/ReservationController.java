package com.example.roomreservation.Controllers;

import com.azure.core.annotation.Get;
import com.example.roomreservation.ModelDTO.*;
import com.example.roomreservation.Security.service.TokenProvider;
import com.example.roomreservation.Services.ReservationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@CrossOrigin
public class ReservationController {

    private final     ReservationService reservationService;
    private final TokenProvider      tokenProvider;

    @ExceptionHandler(IllegalArgumentException.class)
    protected ResponseEntity<String> handleEntityNotFound(
            IllegalArgumentException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.CONFLICT);
    }

    @PostMapping("/reservation/save")
    public ResponseEntity<ReservationDtoDetails> saveReservation(@RequestHeader("authorization") String token, @RequestBody ReservationDto reservationDto){
        String user = tokenProvider.getUsernameFromToken(TokenProvider.getTokenFromHeader(token));
        return new ResponseEntity<>(reservationService.saveReservation(reservationDto, user), HttpStatus.CREATED);
    }

    @GetMapping("/reservations")
    public ResponseEntity<Collection<ReservationDtoDetails>> getReservation(@RequestParam Long id){
        return new ResponseEntity<>(reservationService.getReservations(id), HttpStatus.OK);
    }

    @PostMapping("/reservations/room")
    public ResponseEntity<Collection<ReservationDtoDetails>> getReservationForRoomInDates(@RequestBody ReservationRoomDto reservationRoomDto){
        return new ResponseEntity<>(reservationService.getReservations(reservationRoomDto), HttpStatus.OK);
    }

    @PostMapping("/reservations/floor")
    public ResponseEntity<Collection<IsRoomReserved>> getReservationForFloorInDates(@RequestBody FloorReservationsInDatesDto floorReservationsInDatesDto){
       return new ResponseEntity<>(reservationService.getReservations(floorReservationsInDatesDto), HttpStatus.OK);
    }

    @GetMapping("/reservations/user")
    public ResponseEntity<Collection<ReservationDtoDetailsWithRoom>> getReservationForUser(@RequestHeader("authorization") String token){
        String user = tokenProvider.getUsernameFromToken(TokenProvider.getTokenFromHeader(token));
        return new ResponseEntity<>(reservationService.getFutureUserReservations(user), HttpStatus.OK);
    }
}
