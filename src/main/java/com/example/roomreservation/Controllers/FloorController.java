package com.example.roomreservation.Controllers;

import com.example.roomreservation.Services.FloorService;
import com.example.roomreservation.Services.FloorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class FloorController {
    private final FloorService floorService;

    public FloorController(FloorService floorService) {
        this.floorService = floorService;
    }

    @GetMapping(value = "/floor")
    public ResponseEntity<Object> getFloor(@RequestParam Long number){
        return new ResponseEntity<>(floorService.getFloor(number), HttpStatus.OK);
    }

    @GetMapping("/")
    public String index() {
        return "Welcome to the Floors Page!";
    }
}
