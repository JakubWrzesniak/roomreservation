package com.example.roomreservation.Controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.roomreservation.ModelDTO.RoleToUserForm;
import com.example.roomreservation.ModelDTO.UserDto;
import com.example.roomreservation.Security.service.TokenProvider;
import com.example.roomreservation.Services.IUserService;
import com.example.roomreservation.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.*;

import static java.util.Arrays.stream;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@CrossOrigin
public class UserController {
    private final IUserService userService;
    @Autowired
    private final TokenProvider tokenProvider;

    @GetMapping("/authenticate")
    public ResponseEntity<Void> authenticate(@RequestHeader("authorization") String token){
        if(userService.authenticateUser(token)){
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @GetMapping("/users")
    public ResponseEntity<Collection<UserDto>> getUsers(){
        return ResponseEntity.ok().body(userService.getUsers());
    }
}
