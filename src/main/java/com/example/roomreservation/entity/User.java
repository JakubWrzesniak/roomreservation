package com.example.roomreservation.entity;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.validator.constraints.UniqueElements;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity(name="Users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @NotEmpty
    @NonNull
    @Column(name="username", nullable = false, length = 100)
    private String username;

    @Email
    @NotEmpty
    @NonNull
    @Column(name="email_address", nullable = false, length = 100, unique = true)
    private String emailAddress;

    @OneToMany(mappedBy = "reservationId", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private Collection<Reservation> reservedRooms;

    @Column(name="azure_id", unique = true)
    private String userAzureId;

    @ManyToMany(mappedBy = "participants", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    private Set<Reservation> meetings = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        User user = (User) o;
        return userId != null && Objects.equals(userId, user.userId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
