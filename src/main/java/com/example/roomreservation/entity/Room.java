package com.example.roomreservation.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name="rooms")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "room_id")
    private Long roomId;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "floor_number", nullable = false)
    @ToString.Exclude
    @JsonBackReference
    private Floor floor;

    @NotNull
    @NotEmpty
    @Column(name="room_number", length = 50, nullable = false)
    private String number;

    @NotEmpty
    @Column(name="room_name", length = 100)
    private String name;


    @OneToMany(mappedBy = "reservationId", cascade = CascadeType.ALL, orphanRemoval = true)
    @ToString.Exclude
    private Collection<Reservation> reservations;

    @Min(0)
    @Max(2000)
    @Column(name = "room_pos_x")
    private Double x;

    @Min(0)
    @Max(2000)
    @Column(name = "room_pos_y")
    private Double y;

    @Min(0)
    @Max(2000)
    @Column(name = "room_width")
    private Double width;

    @Min(0)
    @Max(2000)
    @Column(name = "room_height")
    private Double height;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Room room = (Room) o;
        return roomId != null && Objects.equals(roomId, room.roomId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
