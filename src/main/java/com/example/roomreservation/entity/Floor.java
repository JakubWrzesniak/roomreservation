package com.example.roomreservation.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Setter
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name="floors")
public class Floor {
    @Id
    @Column(name = "floor_number", nullable = false)
    private Long number;

    @NotNull
    @Min(1)
    @Max(2000)
    @Column(name="floor_height", length = 100)
    private Integer height;

    @NotNull
    @Min(1)
    @Max(2000)
    @Column(name="floor_width", length = 100)
    private Integer width;

    @OneToMany(mappedBy = "floor", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @ToString.Exclude
    @JsonManagedReference
    private Set<Room> rooms = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Floor floor = (Floor) o;
        return number != null && Objects.equals(number, floor.number);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
